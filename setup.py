from distutils.core import setup

setup(
    name='package_logger',
    version='0.0.1',
    packages=[''],
    url='',
    license='MIT',
    author='Tobias Macey',
    author_email='tmacey@boundlessnotions.com',
    description='A simple module for centralized logging in a package'
)
