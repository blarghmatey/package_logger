"""
Set up a logger instance to be used within the package.
"""
import logging
import logging.handlers

import os

level_dict = {
    'debug': logging.DEBUG,
    'info': logging.INFO,
    'warning': logging.WARNING,
    'error': logging.ERROR,
    'critical': logging.CRITICAL,
    'fatal': logging.FATAL
}

def build_logger(log_level='info', log_path=os.getcwd(), log_size=1048576,
                  log_count=30):
    """
    Configures a logger object to be used at the package level. Instantiating
    within a packages __init__.py will allow for centralized logging. at the
    module level use the following approach:
        import logging
        logger = logging.getLogger(__name__)

    :param log_level: log level to use
    :param log_path: directory that the log file should be written to
    :param log_size: how large an individual log file should get before being
        rotated
    :param log_count: how many rotated logs to keep on disk
    """
    log_level = level_dict[log_level]
    log_path = log_path
    max_size = log_size
    backup_count = log_count

    logger = logging.getLogger(__package__)
    logger.setLevel(log_level)
    handler = logging.handlers.RotatingFileHandler('{}/gps_sensor.log'.format(
                                                   log_path), maxBytes=max_size,
                                                   backupCount=backup_count)
    handler.setLevel(log_level)
    formatter = logging.Formatter(
        fmt="%(asctime)s: %(module)s - %(levelname)s -- %(message)s")
    if log_level == logging.DEBUG:
        debug_handler = logging.StreamHandler()
        debug_handler.setLevel(logging.DEBUG)
        debug_handler.setFormatter(formatter)
        logger.addHandler(debug_handler)
    handler.setFormatter(formatter)
    logger.addHandler(handler)
